// Fill out your copyright notice in the Description page of Project Settings.


#include "SingleCharacter.h"
#include "Components/InputComponent.h"
//#include "EnhancedInputComponent.h"
//#include "EnhancedInputSubsystems.h"

// Sets default values
ASingleCharacter::ASingleCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASingleCharacter::BeginPlay()
{
	Super::BeginPlay(); 

	
	
}

// Called every frame
void ASingleCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASingleCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASingleCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASingleCharacter::MoveRight);

}

void ASingleCharacter::MoveForward(float value)
{
	FVector forwardVector = GetActorForwardVector();
	AddMovementInput(forwardVector, value);
}

void ASingleCharacter::MoveRight(float value)
{
	FVector rightVector = GetActorRightVector();
	AddMovementInput(rightVector, value);
}

